require("state")

------------------------------------SW text-----------------------------
SWText = {}
SWText.__index = SWText
setmetatable(SWText, {__index =  State})

function SWText.new()
	local swtext = State.new()
  setmetatable(swtext, SWText) 

  swtext.pShader = [[
extern number screenWidth;
vec4 effect(vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords)
{
  vec4 pixel = Texel(texture, texture_coords );
  if (screen_coords.y < 100.0)
    pixel.a = 0;
  else if (screen_coords.y < 256.0) {
    float factor = (screen_coords.y-100.0) / 100.0;
	pixel.a *= factor * factor;
  }
  
  return pixel;
}
	  ]]
  swtext.vShader = [[
		vec4 position(mat4 transform_projection, vec4 vertex_position)
		{/*
			float PHIz = 0.4,
				  n = -100,
			      f = 100,
				  r = 0,
				  l = 800,
				  t = 0,
				  b = 600,
				  d = 1.0/(tan(PHIz/2.0));
		    mat4 proj = mat4(2/(r-l),      0,             0,           0,
							 0,            2/(t-b),       0,           0,
							 0,            0,            -2/(f-n),     0,
							 -(r+l)/(r-l), -(t+b)/(t-b), -(f+n)/(f-n), 1);*/
			float PHIz = 1,
				  n = 10,
			    f = 6000,
				  r = 0,
				  l = 800,
				  t = 0,
				  b = 600,
				  a = 0.8,
				  d = 1.0/(tan(PHIz/2.0));
		    mat4 proj = mat4(d/a,      0,       0,           0,
							 0,        d,       0,           0,
							 0,        0,      (n+f)/(n-f), -1,
							 0,        0,       2*n*f/(n-f),   0);
			float phi = 3.8;
			mat4 rot = mat4( 1,      	   0,             0,           0,
							 0,            cos(phi),      -sin(phi),   0,
							 0,            sin(phi),      cos(phi),    0,
							 -300,          300,        -850,      1);
			rot = proj*rot;
		    return rot * vertex_position;
		}
	]]
	swtext.effect = nil
	swtext.canvas = love.graphics.newCanvas(800, 1200)
	swtext.font = love.graphics.newFont("resources/DejaVuSans.ttf", 18)
	swtext.position = 0
	swtext.interImg = love.graphics.newImage("resources/gfx/Interbrig.png")
	swtext.source = love.audio.newSource("resources/audio/lainternational.ogg")
	swtext.effect = love.graphics.newShader(swtext.pShader, swtext.vShader)
	swtext.textspeed = 11
  
  return swtext
end

function SWText.init(self)
	love.window.setMode(800, 600)
	love.graphics.setFont(self.font)
	self.position = 0
	self.source:play()
end

function SWText.draw(self)
		love.graphics.draw(self.interImg, 0, 0)
		love.graphics.setCanvas(self.canvas)
		love.graphics.setColor(255, 255, 255, 255)
		love.graphics.clear()
		love.graphics.setBackgroundColor(200, 0, 0, 255)
		love.graphics.translate(0, -self.position)
		love.graphics.printf(FN.messages.crawtitle, 105, 720, 395, "center")
		love.graphics.printf(FN.messages.crawling,
		  105, 780, 395, "justify")
		love.graphics.setCanvas()
		love.graphics.setShader(self.effect)
		love.graphics.draw(self.canvas)
		love.graphics.setShader()
end

function SWText:updateState(dt)
	self.position = self.position+self.textspeed*dt
end

function SWText:keypressed(key, scancode, isrepeat)
  if key == 'space' then
    self.source:stop()
    setGameMode(menu)
	end
end
------------------------------------------------------------------------
