require("flying_nations")

FN.messages_en = {
  crawtitle = [[
EPISODE I
ATTACK OF THE CHATOS]],
  crawling =[[
Year 1936. Turmoil has engulfed the
Iberian peninsula. It is a period of
civil war between defenders of the 2nd
Spanish Republic and the military
rebellion, inspired and supported by
the German and Italian fascist
governments.                  

Rebel troops under the leadership of
the ruthless General Franco win one
battle after another. Sinister legion
"Condor", formed from the best pilots
of the German Luftwaffe, and Italian
Royal Air Force reigns supreme in the
sky.

In a desperate attempt to save
the young Spanish republic, communists
and anti-fascists from all over the
World come to volunteer International
Brigades. USSR sends the latest
military equipment and experts to
struggle against the fascism. Among
them the Polikarpov's I-15 "Chato"
biplane fighter aircrafts came to Spain
in order to stop the reign of the
legion "Condor"...               
]],
  i15model = 'I-15 "Chato"',
  i207model = 'I-207/3 Prototype'
}
