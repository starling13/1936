require("state")

-------------------------------LogoMode---------------------------------
LogoMode = {}
LogoMode.__index = LogoMode
setmetatable(LogoMode, {__index = State})

LogoMode.pShader = [[

extern vec2 mouse;

vec4 effect( vec4 color, Image texture, vec2 texture_coords, vec2 screen_coords )
{
		vec4 texcolor = Texel(texture, texture_coords);
		vec2 delta = mouse - screen_coords;
		float len = length(delta);
		float exp1 = (pow(2.71, -0.0003*len*len));
		float exp2 = (pow(2.71, -0.0001*len*len));
		float exp3 = exp2 - 0.4*exp1;
		texcolor.rgb *= 0.45 + exp3;
		
		return (texcolor * color);
}

]]

function LogoMode.new()
	local lM = State.new()
  setmetatable(lM, LogoMode)
  
  lM.background = love.graphics.newImage("resources/gfx/1936_1.png")
  lM.pShader = love.graphics.newShader(LogoMode.pShader);
	return lM
end

function LogoMode.init(self)
	love.window.setMode(800, 600)
  love.graphics.setCanvas()
end

function LogoMode:draw()
	love.graphics.setShader(self.pShader)
	local x,y = love.mouse.getPosition()
	self.pShader:send('mouse', {x,y})
	love.graphics.draw(self.background)
	love.graphics.setShader()
end

function LogoMode:updateState(dt)
  -- do nothing
end

function LogoMode:keypressed(key, scancode, isrepeated)
  if key == 'escape' then
		love.event.push('quit')
	elseif key == 'space' then
		setGameMode(swText)
	end
end
------------------------------------------------------------------------
