require("start_screen")
require("intro_mode")
require("vscroll_mode")
require("menu_mode")
require("timer")
require("aircrafts")
require("lan_mode")

logoState = nil
vscroll = nil
swText = nil
menu = nil
languageSelectState = nil

-- timers list
timers = {}

-- timeout callback for double click detection
function doubleTmeout(key)
	if timers[key] then
		timers[key]:stop()
	end
end

function setGameMode(mode)
	mode:init()
	gameMode = mode
end

function love.load()
  if arg[#arg] == "-debug" then
    require("mobdebug").start()
  end
  
	math.randomseed(os.time())
  
  languageSelectState = FN.LanguageMode.new()
	logoState = LogoMode.new()
	vscroll = VScrollGame.new()
	swText = SWText.new()
  menu = Menu.new()
  
	timers["left"] = FN.Timer.new(0.3, doubleTmeout, "left")
	timers["right"] = FN.Timer.new(0.3, doubleTmeout, "right")
	-- start mode
	setGameMode(languageSelectState)
end

function love.draw()
	gameMode:draw()
end

function love.keypressed(key, scancode, isrepeat)
	gameMode:keypressed(key, scancode, isrepeat)
  if timers[key] then
		if timers[key]:isRunning() then
      gameMode:doubleKeyPressed(key)  
		end
	end
end

function love.keyreleased(key)
  gameMode:keyreleased(key)
	if timers[key] then
		timers[key]:start()
	end
end

function love.update(dt)
	for key, timer in pairs(timers) do
		timer:update(dt)
	end
	gameMode:update(dt)
end
