require ("timer")
require ("flying_nations")

-----------------------------------Vessel-------------------------------
Aircraft = {}
Aircraft.__index = Aircraft
setmetatable(Aircraft,
  {__call = function (cls, ...)
      local self = setmetatable({}, cls)
      self:_init(...)
      return self
    end
  })

function Aircraft:_init(model)
	self.speed = 0
	self.img = {}
  self.show = {}
  self.model = model
  
  for i=36,71 do
		local num = '' .. i
		for j=0,3-string.len(num) do
			num = '0' .. num
		end
    local fname = 'resources/gfx/' .. self.model .. '/show/'
		  .. num .. '.png'
    if love.filesystem.exists(fname) then
      print ('loading image ', fname)
      self.show[i-36] = love.graphics.newImage(fname)
    else
      print ('Cannot load ', fname, f)
    end
  end
  
  for i=0,35 do
		local num = '' .. i
    for j=0,3-string.len(num) do
			num = '0' .. num
		end
    local fname = 'resources/gfx/' .. self.model .. '/top/' .. num .. '.png'
    local f = io.open(fname, 'r')
    if love.filesystem.exists(fname) then
      print ('loading image ', fname)
      self.img[i] = love.graphics.newImage(fname)
    end
	end
  
  if self.img[0] ~= nil then
    self.width = (self.img[0]):getWidth()
    self.height = (self.img[0]):getHeight()
  end
end

function Aircraft.instance(env)
  local res = AircraftInstance.new(env, self)
  return res
end

AircraftInstance = {}
AircraftInstance.__index = AircraftInstance

function AircraftInstance.new(game, vessel)
  local vi = setmetatable({}, AircraftInstance)
  vi.vessel = vessel
  vi.x = 0
  vi.y = 0
  vi.width = vessel.width
  vi.height = vessel.height
  vi.health = vessel.health
  vi.debris = game.debris:clone()
	vi.sparkles = game.sparkles:clone()
	vi.fire = game.fireGenerator:clone()
	vi.smoke = game.smokeGenerator:clone()
  vi.img = 0
  vi.guns = {}
  vi.game = game
  
  return vi
end

function AircraftInstance:shootOn()
  for mgun in pairs(self.guns) do
    mgun:on()
  end
end

function AircraftInstance:shootOff()
  for mgun in pairs(self.guns) do
    mgun:off()
  end
end

function AircraftInstance:draw()
  
	love.graphics.draw(self.vessel.img[self.img], self.x, self.y)
	if (self.health > 0) then
		love.graphics.draw(self.sparkles, self.x+self.vessel.width/2,
			self.y+3*self.vessel.height/4)
		love.graphics.draw(self.debris, self.x+self.vessel.width/2,
			self.y+self.vessel.height/2)
	else
		love.graphics.draw(self.smoke, self.x + (self.vessel.width/2),
			self.y+self.vessel.height/2)
		love.graphics.draw(self.fire, self.x + (self.vessel.width/2),
			self.y+self.vessel.height/2)
	end
end

------------------------------------------------------------------------

FiatCR32 = {}
FiatCR32.__index = FiatCR32
setmetatable(FiatCR32, {__index = Aircraft})

function FiatCR32.new()
	local res = Aircraft("FiatCR32")
  setmetatable(res, FiatCR32)
	res.health = 50
  
	return res
end

function FiatCR32:instance(env)
  local res = AircraftInstance.new(env, Aircraft.fiatcr32)
  local mg = MachineGun.new(res, -3, 0, 0.104)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, -6, 0, 0.104)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  
  return res
end

------------------------------------------------------------------------

Heinkel51 = {}
Heinkel51.__index = Heinkel51
setmetatable(Heinkel51, {__index = Aircraft})

function Heinkel51.new()
	local result = Aircraft("Heinkel51")
  setmetatable(result, Heinkel51)
	result.health = 40
	return result
end

function Heinkel51:instance(env)
  local res = AircraftInstance.new(env, Aircraft.heinkel51)
  local mg = MachineGun.new(res, -3, 0, 0.104)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, -6, 0, 0.104)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  
  return res
end

------------------------------------------------------------------------

I15 = {}
I15.__index = I15
setmetatable(I15, {__index = Aircraft})

function I15.new()
	local result = Aircraft("I15")
  setmetatable(result, I15)
  result.modelName = FN.messages.i15model
	result.health = 30
  result.mgunsnd = love.audio.newSource("resources/audio/machinegun_750.ogg", "static")
  result.mgunsnd:setLooping(true)
  
	return result
end

function I15:instance(env)
  local res = AircraftInstance.new(env, Aircraft.i15)
  local mg = MachineGun.new(res, -3, 0, 0.08)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, -6, 0, 0.08)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, 3, 0, 0.08)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, 6, 0, 0.08)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  
  return res
end

I207 = {}
I207.__index = I207
setmetatable(I207, {__index = Aircraft})

function I207.new()
	local result = Aircraft("I207")
  setmetatable(result, I207)
  result.modelName = FN.messages.i207model
	result.health = 40
  result.mgunsnd = love.audio.newSource("resources/audio/mgun_762x2.ogg", "static")
  result.mgunsnd:setLooping(true)
  
	return result
end

function I207:instance(env)
  local res = AircraftInstance.new(env, Aircraft.i207)
  local mg = MachineGun.new(res, -3, 0, 0.036)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, -6, 0, 0.036)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, 3, 0, 0.036)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  mg = MachineGun.new(res, 6, 0, 0.036)
  res.guns[mg] = true
  vscroll:addTimer(mg.timer)
  
  return res
end

-------------------------------- MachineGun ------------------------------------
MachineGun = {}
MachineGun.__index = MachineGun
setmetatable(MachineGun, { _index = MachineGun })
  
function MachineGun.new(vesselinstance, x, y, rate)
  local mg = {}
  setmetatable(mg, MachineGun)
  rate = math.random((rate-rate/10)*10000, (rate+rate/10)*10000)
  mg.timer = FN.Timer.new(rate/10000, MachineGun.shoot, mg)
  mg.x = x
  mg.y = y
  mg.vesselinstance = vesselinstance
  
  return mg
end

function MachineGun:on()
  self.timer:start()
end

function MachineGun:off()
  self.timer:stop()
end

function MachineGun:shoot()
  local newBullet = { x = self.x + self.vesselinstance.x + self.vesselinstance.width/2,
                      y = self.y + self.vesselinstance.y,
                      speed = 1000, img = self.vesselinstance.game.bulletImg }
  self.vesselinstance.game.bullets[newBullet] = true
end
