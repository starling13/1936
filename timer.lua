--------------------------------------------------------------------------------
-- File: timer.lua
-- Generated: 25--2016 17:21:46
--------------------------------------------------------------------------------

require ("flying_nations")

--------------------------------------------------------------------------------
-- @class FN.Timer
-- 
FN.Timer = {}

-- @Operation FN.Timer.new
function FN.Timer.new(period, callback, arg)
	local timer = {
    -- @attribute _maxValue
    _maxValue = nil,
    -- @attribute _value
    _value = 0,
    -- @attribute _run
    _run = false,
    -- @attribute _callback
    _callback = nil,
    -- @attribute _arg
    _arg = nil
  }
	setmetatable(timer, {__index = FN.Timer})
	
	timer._maxValue = period
	timer._value = timer._maxValue
	timer._callback = callback
	timer._arg = arg
	
	return timer
end

-- @Operation FN.Timer.start
function FN.Timer:start()
	self._run = true
end

-- @Operation FN.Timer.stop
function FN.Timer:stop()
	self._run = false
end

-- @Operation FN.Timer.isRunning
function FN.Timer:isRunning()
	return self._run
end

-- @Operation FN.Timer.getValue
function FN.Timer:getValue()
	return self._value
end

-- @Operation FN.Timer.update
function FN.Timer:update(dt)
	if self._run then
		self._value = self._value - dt;
		if self._value < 0 then
			self._value = self._maxValue
			if self._callback then
				if self._arg then
					self._callback(self._arg)
				else
					self._callback()
				end
			end
		end
	end
end

--------------------------------------------------------------------------------
