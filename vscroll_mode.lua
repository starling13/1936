require ("timer")
require ("state")
require ("aircrafts")

local fps = 0

canShoot = true
canShootTimerMax = 0.05
canShootTimer = canShootTimerMax

--More timers
createEnemyTimerMax = 4
createEnemyTimer = createEnemyTimerMax

-- Collision detection taken function from http://love2d.org/wiki/BoundingBox.lua
-- Returns true if two boxes overlap, false if they don't
-- x1,y1 are the left-top coords of the first box, while w1,h1 are its width and height
-- x2,y2,w2 & h2 are the same, but for the second box
function CheckCollision(x1,y1,w1,h1, x2,y2,w2,h2)
  return x1 < x2+w2 and
  x2 < x1+w1 and
  y1 < y2+h2 and
  y2 < y1+h1
end

--------------------------------Player----------------------------------
Player = {}
Player.__index = Player
setmetatable(Player, {__index = Player})

function Player.new()
  local player = {
    speed = 250,
    ptype="I15",
    img = {},
    position = 0,
    angle = 0,
    width = 96,
    height = 64,
    turnTimer = nil,
    turnUpdate = Player.turnUpdate,
    state = 0
  }
  setmetatable(player, Player)
  player.vessel = Aircraft.i15
  player.turnTimer = FN.Timer.new(0.03, Player.turnUpdate, player)
  return player
end

function Player:turnUpdate()
  if self.state == -1 then
    if self.angle == 0 then
      self.angle = 35
    elseif self.angle == 1 then
      self.angle = 0
      self.state = 0
      self.turnTimer:stop()
    else
      self.angle = self.angle - 1
    end
    return
  elseif self.state == 1 then
    if self.angle >= 35 then
      self.angle = 0
      self.state = 0
      self.turnTimer:stop()
    else
      self.angle = self.angle + 1
    end
    return
  end
  if love.keyboard.isDown('left','a') then
    if self.angle == 0 then
      self.angle = 35
    else
      self.angle = self.angle - 1
    end
    if self.angle < 30 then
      self.angle = 30
    end
  elseif love.keyboard.isDown('right','d') then
    self.angle = self.angle + 1
    if self.angle > 6 then
      self.angle = 6
    end
  else
    if self.angle >= 30 then
      self.angle = self.angle + 1
      if self.angle >= 36 then
        self.angle = 0
        self.turnTimer:stop()
      end
    else
      self.angle = self.angle - 1
      if self.angle <= 0 then
        self.angle = 0
        self.turnTimer:stop()
      end
    end
  end
  if self.angle < 0 or self.angle > 35 then
    print("Player:turnUpdate:ERROR:", self.angle)
  end
end

function Player.draw(self)
  love.graphics.draw(self.vessel.img[self.angle], self.vesselInstance.x, self.vesselInstance.y)
end

---------------------------------VScrollGame----------------------------
VScrollGame = {}
VScrollGame.__index = VScrollGame
setmetatable(VScrollGame, {__index = State})

function VScrollGame.new()
  local vScrollGame = State.new()
  setmetatable(vScrollGame, VScrollGame)

  vScrollGame.enemies = {}
  vScrollGame.broken_enemies = {}
  vScrollGame.score = 0
  vScrollGame.bullets = {}
  vScrollGame.bulletImg = nil
  vScrollGame.enemyImg = nil
  vScrollGame.groundImg = nil
  vScrollGame.isAlive = true
  vScrollGame.score = 0
  vScrollGame.clouds1 = nil
  vScrollGame.fireGenerator = nil
  vScrollGame. motorsnd = love.audio.newSource( "resources/audio/motor.ogg", "static" )
  vScrollGame.motorPitch = 1.3

  vScrollGame.dispenser = nil
  vScrollGame.smokeGenerator = nil

  vScrollGame.bulletImg = love.graphics.newImage('resources/gfx/bullet.png')
  vScrollGame.groundImg = love.graphics.newImage('resources/gfx/ground.png')

  -- global fire generator
  local img = love.graphics.newImage('resources/gfx/fire.png')
  vScrollGame.fireGenerator = love.graphics.newParticleSystem(img, 256)
  vScrollGame.fireGenerator:setParticleLifetime(0.4, 1)
  vScrollGame.fireGenerator:setEmissionRate(64)
  vScrollGame.fireGenerator:setSizes(0.5, 0.7)
  vScrollGame.fireGenerator:setSizeVariation(1)
  vScrollGame.fireGenerator:setAreaSpread( "normal", 1, 2 )
  vScrollGame.fireGenerator:setLinearAcceleration(-40, -170, 40, -220) -- Random movement in all directions.
  vScrollGame.fireGenerator:setColors(255, 255, 255, 127, 255, 0, 0, 0) -- Fade to transparency.

  -- global sparkles generator
  vScrollGame.sparkles = love.graphics.newParticleSystem(img, 256)
  vScrollGame.sparkles:setParticleLifetime(0.1, 0.2)
  vScrollGame.sparkles:setEmissionRate(64)
  vScrollGame.sparkles:setSizes(0.2, 0.5)
  vScrollGame.sparkles:setSizeVariation(1)
  vScrollGame.sparkles:setAreaSpread( "normal", 20, 3 )
  vScrollGame.sparkles:setLinearAcceleration(-40, -170, 40, -220) -- Random movement in all directions.
  vScrollGame.sparkles:setColors(255, 255, 255, 255, 255, 255, 255, 255) -- Fade to transparency.

  img = love.graphics.newImage('resources/gfx/smoke.png')
  vScrollGame.smokeGenerator = love.graphics.newParticleSystem(img, 512)
  vScrollGame.smokeGenerator:setParticleLifetime(3.5, 4.7) -- Particles live at least 2s and at most 5s.
  vScrollGame.smokeGenerator:setEmissionRate(128)
  vScrollGame.smokeGenerator:setSizes(0.7, 1.2)
  vScrollGame.smokeGenerator:setSizeVariation(1)
  vScrollGame.smokeGenerator:setSpeed(60, 120)
  vScrollGame.smokeGenerator:setDirection( -math.pi/2 )
  vScrollGame.smokeGenerator:setTangentialAcceleration(-6, 6)
  vScrollGame.smokeGenerator:setColors(255, 255, 255, 127, 255, 255, 255, 0) -- Fade to transparency.

  vScrollGame.debris = love.graphics.newParticleSystem(img, 256)
  vScrollGame.debris:setParticleLifetime(2, 3)
  vScrollGame.debris:setEmissionRate(128)
  vScrollGame.debris:setSizes(0.2, 0.3)
  vScrollGame.debris:setSizeVariation(1)
  vScrollGame.debris:setSpeed(300, 350)
  vScrollGame.debris:setDirection( -math.pi/2 )
  vScrollGame.debris:setAreaSpread("normal", 30, 6)
  vScrollGame.debris:setTangentialAcceleration(-3, 3)
  vScrollGame.debris:setColors(255, 255, 255, 255, 255, 255, 255, 0) -- Fade to transparency.

  -- global cloaud generators
  img = love.graphics.newImage('resources/gfx/cloud.png')
  vScrollGame.clouds = love.graphics.newParticleSystem(img, 32)
  vScrollGame.clouds:setParticleLifetime(30, 30)
  vScrollGame.clouds:setEmissionRate(0.4)
  vScrollGame.clouds:setSizes(0.5, 1.5)
  vScrollGame.clouds:setSizeVariation(1)
  vScrollGame.clouds:setSpeed(30, 30)
  vScrollGame.clouds:setDirection(math.pi/2)
  vScrollGame.clouds:setSpin(0.2, 0.4)
  vScrollGame.clouds:setAreaSpread("uniform", 450, 0)

  vScrollGame.clouds1 = love.graphics.newParticleSystem(img, 16)
  vScrollGame.clouds1:setParticleLifetime(20, 20)
  vScrollGame.clouds1:setEmissionRate(0.3)
  vScrollGame.clouds1:setSizes(2, 3)
  vScrollGame.clouds1:setSizeVariation(1)
  vScrollGame.clouds1:setSpeed(150, 180)
  vScrollGame.clouds1:setDirection(math.pi/2)
  vScrollGame.clouds:setSpin(0.2, 0.4)
  vScrollGame.clouds1:setAreaSpread("uniform", 450, 0)

  vScrollGame.clouds2 = love.graphics.newParticleSystem(img, 16)
  vScrollGame.clouds2:setParticleLifetime(2, 2)
  vScrollGame.clouds2:setEmissionRate(0.2)
  vScrollGame.clouds2:setSizes(5, 7)
  vScrollGame.clouds2:setSizeVariation(1)
  vScrollGame.clouds2:setSpeed(600, 800)
  vScrollGame.clouds2:setDirection(math.pi/2)
  vScrollGame.clouds2:setAreaSpread("uniform", 450, 0)

  vScrollGame.motorsnd:setLooping(true)
  
  vScrollGame.player = Player.new()

  return vScrollGame
end

function VScrollGame:doubleKeyPressed(key)
  if key == 'left' then
    self.player.state = -1
  elseif 
  key == 'right' then
    self.player.state = 1
  end
end

function VScrollGame:init()
  self.dispenser = TargetDispenser.new(self)
  self.dispenser:addTarget(Aircraft.fiatcr32, 30)
  self.dispenser:addTarget(Aircraft.heinkel51, 70)
  self:addTimer(self.player.turnTimer)

  love.window.setMode(600, 800)
  love.graphics.setCanvas()
  self.motorsnd:play()
end

function VScrollGame:draw()
  if self.isAlive then
    love.graphics.setCanvas(canvas)
    love.graphics.draw(self.groundImg, 0, -self.groundImg:getHeight()
      +love.graphics:getHeight()+self.player.position)

    love.graphics.print("SCORES: " .. self.score, 10, 10)
    love.graphics.print("FPS: " .. 1/fps, 300, 10)

    love.graphics.draw(self.clouds, love.graphics:getWidth()/2, -100)
    love.graphics.draw(self.clouds1, love.graphics:getWidth()/2, -100)
    for bullet,v in pairs(self.bullets) do
      love.graphics.draw(bullet.img, bullet.x, bullet.y, 0, 1, 1, 3)
    end
    for i, enemy in ipairs(self.enemies) do
      enemy:draw()
    end
    for enemy in pairs(self.broken_enemies) do
      enemy:draw()
    end
    self.player:draw();
    love.graphics.draw(self.clouds2, love.graphics:getWidth()/2, -160)
  else
    love.graphics.print("Press 'R' to restart", love.graphics:getWidth()/2-50,
      love.graphics:getHeight()/2-10)
  end 
end

function VScrollGame:keypressed(key, scancode, isrepeated)
  if key == 'escape' then
    self.motorsnd:stop()
    setGameMode(menu)
  elseif key == 'lctrl' then
    self.player.vesselInstance:shootOn()
  else
    print (key)
  end
end

function VScrollGame:keyreleased(key)
  if key == 'lctrl' then
    self.player.vesselInstance:shootOff()
  end
end

function VScrollGame:updateState(dt)

  if fps ~= 0 then
    fps = (fps + dt) / 2
  else
    fps = dt
  end

  if love.keyboard.isDown('r') and not self.isAlive then
    vscroll = VScrollGame.new()
    vscroll:init()
  end

  if not self.isAlive then
    return
  end

  self.motorsnd:setPitch( self.motorPitch )

  self.clouds:update(dt)
  self.clouds1:update(dt)
  self.clouds2:update(dt)

  self.player.position = self.player.position + 10*dt

  if love.keyboard.isDown('left','a') then
    self.player.turnTimer:start();
    if self.player.vesselInstance.x > 0 then -- binds us to the map
      self.player.vesselInstance.x = self.player.vesselInstance.x - (self.player.speed*dt)
    end
  elseif love.keyboard.isDown('right','d') then
    self.player.turnTimer:start();
    if self.player.vesselInstance.x < (love.graphics.getWidth() - self.player.width) then
      self.player.vesselInstance.x = self.player.vesselInstance.x + (self.player.speed*dt)
    end
  end

  self.dispenser:update(dt)

  if love.keyboard.isDown('up','w') then
    if self.motorPitch < 2 then
      self.motorPitch = self.motorPitch + self.motorPitch*dt/2
    end
    if self.player.vesselInstance.y > 0 then -- binds us to the map
      self.player.vesselInstance.y = self.player.vesselInstance.y - (self.player.speed*dt)
    end
  elseif love.keyboard.isDown('down','s') then
    if self.motorPitch > 0.6 then
      self.motorPitch = self.motorPitch - self.motorPitch*dt/2
    end
    if self.player.vesselInstance.y < (love.graphics.getHeight() - self.player.height) then
      self.player.vesselInstance.y = self.player.vesselInstance.y + (self.player.speed*dt)
    end
  else
    if self.motorPitch > 1.3 then
      self.motorPitch = self.motorPitch - self.motorPitch*dt/2
    elseif self.motorPitch < 1.3 then
      self.motorPitch = self.motorPitch + self.motorPitch*dt/2
    end
  end

  if love.keyboard.isDown(' ', 'rctrl', 'lctrl', 'ctrl') then
    if not self.player.vessel.mgunsnd:isPlaying() then
      self.player.vessel.mgunsnd:play()
    end

  else
    if self.player.vessel.mgunsnd:isPlaying() then
      self.player.vessel.mgunsnd:stop()
    end
  end

  -- update the positions of bullets
  for bullet,v in pairs(self.bullets) do
    bullet.y = bullet.y - (bullet.speed * dt)

    if bullet.y < 0 then -- remove bullets when they pass off the screen
      self.bullets[bullet] = nil
    end
  end

  -- update the positions of enemies
  for i, enemy in ipairs(self.enemies) do
    enemy.y = enemy.y + (enemy.speed * dt)
    enemy.debris:update(dt)
    enemy.debris:pause()
    enemy.sparkles:update(dt)
    enemy.sparkles:pause()
    if enemy.y > 850 then
      table.remove(self.enemies, i)
    end
  end

  -- update the positions of enemies
  for enemy in pairs(self.broken_enemies) do
    enemy.y = enemy.y + (enemy.speed * dt)

    enemy.fire:update(dt)
    enemy.smoke:update(dt)
    if enemy.y > love.graphics:getHeight()+200 then
      self.broken_enemies[enemy] = nil
    end
  end

  -- run our collision detection
  -- Since there will be fewer enemies on screen than bullets we'll loop them first
  -- Also, we need to see if the enemies hit our player

  local random = math.random
  for i, enemy in ipairs(self.enemies) do
    for bullet,v in pairs(self.bullets) do
      if CheckCollision(enemy.x, enemy.y, enemy.vessel.width, enemy.vessel.height,
        bullet.x, bullet.y, bullet.img:getWidth(), bullet.img:getHeight()) then
        rand = random(0,100)
        if (rand < 10) then -- shoot
          self.bullets[bullet] = nil
          enemy.health = enemy.health-1
          enemy.debris:start()
          enemy.sparkles:start()
        end
        if (enemy.health <= 0) then
          self.broken_enemies[enemy] = true
          table.remove(self.enemies, i)
          self.score = self.score + 10
        end
        self.score = self.score + 1
      end
    end

    if CheckCollision(enemy.x, enemy.y, enemy.vessel.width, enemy.vessel.height,
      self.player.vesselInstance.x, self.player.vesselInstance.y, self.player.vessel.width,
      self.player.vessel.height)
    and self.isAlive then
      table.remove(self.enemies, i)
      self.isAlive = false
      print ("die")
    end
  end

end

-------------------------------Target dispenser-------------------------
TargetDispenser = {}
TargetDispenser.__index = TargetDispenser

function TargetDispenser.new(game)
  local result = {
    world = game,
    targets = {},
    sumProb = 0,
  }
  setmetatable(result, TargetDispenser)

  return result
end

-- Target class, probability
function TargetDispenser:addTarget(target, prob)
  local newTarget = { targetType = target, threshold = self.sumProb+prob }
  self.sumProb = self.sumProb+prob
  table.insert(self.targets, newTarget)
end

function TargetDispenser:update(dt)
-- Time out enemy creation
  createEnemyTimer = createEnemyTimer - (1 * dt)
  if createEnemyTimer < 0 then
    createEnemyTimer = createEnemyTimerMax

    -- Create an enemy
    local randomNumber = math.random(0, self.sumProb)
    for i, target in ipairs(self.targets) do
      if randomNumber<target.threshold then
        local newEnemy = target.targetType:instance(self.world)
        xRand = math.random(0, love.graphics.getWidth() - 100)
        newEnemy.x = xRand
        newEnemy.y = -100
        newEnemy.speed = 100
        table.insert(self.world.enemies, newEnemy)
        break
      end
    end
  end
end

------------------------------------------------------------------------
