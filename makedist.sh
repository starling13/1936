#!/bin/bash

set -x

FILES="./main.lua \
       ./start_screen.lua \
       ./resources/DejaVuSans.ttf \
       ./resources/gfx/Interbrig.png \
       ./resources/audio/motor.ogg \
       ./resources/audio/lainternational.ogg \
       ./resources/gfx/1936_1.png \
       ./resources/gfx/bullet.png \
       ./resources/gfx/ground.png \
       ./resources/gfx/fire.png \
       ./resources/gfx/smoke.png \
       ./resources/gfx/cloud.png \
       ./resources/gfx/flags/USSR_flag.png \
       ./resources/gfx/flags/gb_flag.png \
       ./resources/gfx/flags/spanishrepublic_flag.png"

rm 1936.love
zip 1936.zip $FILES
mv ./1936.zip ./1936.love
