require ("state")
require ("flying_nations")
require ("locale_ru")
require ("locale_en")

FN.LanguageMode = {}
FN.LanguageMode.__index = FN.LanguageMode
setmetatable(FN.LanguageMode, {__index = State})

function FN.LanguageMode.new()
  local lm = State.new()
  setmetatable(lm, FN.LanguageMode)
  lm.languageIndex = 0
  lm.languages = {
    {
      name = 'Русский',
      flag = love.graphics.newImage("resources/gfx/flags/USSR_flag.png")
    },
    {
      name = 'English',
      flag = love.graphics.newImage("resources/gfx/flags/gb_flag.png")
    },
    {
      name = 'Español',
      flag = love.graphics.newImage("resources/gfx/flags/spanishrepublic_flag.png")
    }
  }
  
  return lm
end

function FN.LanguageMode:init()
  love.graphics.setLineWidth(4)
  local font = love.graphics.newFont("resources/DejaVuSans.ttf", 24)
  love.graphics.setFont(font)
  self.languageINdex = 0
end

function FN.LanguageMode:draw()
  love.graphics.printf("Select language", 0, 100, 800, "center")
  for i = 0,2 do
    local x = 50 + 250 * i;
    love.graphics.draw(self.languages[i+1].flag, x, 250)
    love.graphics.printf(self.languages[i+1].name, x, 400, 200, "center")
    if self.languageIndex == i then
      love.graphics.rectangle('line', x-2, 248, 204, 102, 5, 5)
    end
  end
end

function FN.LanguageMode:keypressed(key, scancode, isrepeated)
  if key == 'space' then
    if self.languageIndex == 0 then
      FN.messages = FN.messages_ru
    elseif self.languageIndex == 1 then
      FN.messages = FN.messages_en
    else
      FN.messages = FN.messages_sp
    end
    setGameMode(logoState)
  elseif key == 'right' then
    if self.languageIndex < 2 then
      self.languageIndex = self.languageIndex + 1
    else
      self.languageIndex = 0
    end
  elseif key == 'left' then
    if self.languageIndex > 0 then
      self.languageIndex = self.languageIndex - 1
    else
      self.languageIndex = 2
    end
  end
end
