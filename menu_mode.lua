require ("timer")
require ("state")
require ("aircrafts")

Menu = {}
Menu.__index = Menu
setmetatable(Menu, {__index = State})

function Menu.new()
  local M = State.new()
  setmetatable(M, Menu)
  
  M.state = 0
  M.vessel = {}
  M.timer = FN.Timer.new(0.07, Menu.frame, M)
  M:addTimer(M.timer)
  return M
end

function Menu.frame(self)
  if self.state < 35 then
    self.state = self.state + 1
  else
    self.state = 0
  end
end

function Menu:keypressed(key, scancode, isrepeated)
  if key == 'escape' then
		setGameMode(logoState)
  elseif key == 'space' then
    setGameMode(vscroll)
  elseif key == 'left' then
    if self.current > 0 then
      self.current = self.current - 1
    else
      self.current = self.vessels - 1
    end
  elseif key == 'right' then
    if self.current < self.vessels - 1 then
      self.current = self.current + 1
    else
      self.current = 0
    end
	end
  vscroll.player.vessel = self.vessel[self.current]
  vscroll.player.vesselInstance = vscroll.player.vessel:instance(vscroll)
end

function Menu:init()
  love.window.setMode(800, 600)
  love.graphics.setBackgroundColor(10, 30, 50)
  
  Aircraft.i15 = I15.new()
  Aircraft.i207 = I207.new()
  Aircraft.heinkel51 = Heinkel51.new()
  Aircraft.fiatcr32 = FiatCR32.new()
  
  self.vessel[0] = Aircraft.i15
  self.vessel[1] = Aircraft.i207
  self.vessels = 2
  self.current = 0
  
  self.timer:start()
end

function Menu:destroy()
  self.timer:stop()
end

function Menu:draw()
  local vessel = self.vessel[self.current]
  local img = vessel.show[self.state]
  love.graphics.draw(img, 50, 200)
  love.graphics.rectangle('line', 50, 200, 320, 240, 5, 5)
  love.graphics.print("Model:", 400, 200)
  love.graphics.print(self.vessel[self.current].modelName, 550, 200)
  
  love.graphics.print("Speed:", 400, 230)
  love.graphics.print("Manuverability:", 400, 260)
  love.graphics.print("Weapons:", 400, 290)
end
