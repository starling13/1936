require ("flying_nations")
--------------------------- Game state class ------------------------------
State = {}
State.__index = State

function State.new()
  local state = {}
  setmetatable(state, State)
  
  state.timers = {}
  
  return state
end

function State:update(dt)
  for timer,val in pairs(self.timers) do
    timer:update(dt)
  end
  self:updateState(dt)
end

function State:updateState(dt)
  -- do nothing
end

function State:init()
  -- do nothing
end

function State:draw()
  print ("State:draw()")
  -- do nothing
end

function State:destroy()
 -- do nothing
end

function State:keypressed(key, scancode, isrepeat)
  -- do nothing
end

function State:keyreleased(key, scancode, isrepeat)
  -- do nothing
end

function State:doubleKeyPressed(key)
  -- do nothing
end

function State:addTimer(timer)
  self.timers[timer] = true
end
